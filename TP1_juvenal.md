TP1


# I. Utilisateurs

## 1. Création et configuration
Ajouter un utilisateur à la machine, qui sera dédié à son administration. Précisez des options sur la commande d'ajout pour que:

le répertoire home de l'utilisateur soit précisé explicitement, et se trouve dans /home

le shell de l'utilisateur est /bin/bash

* Ajoute un utilisateur
```bash
useradd -d /home/toto

#useradd toto -m -s /bin/bash
#cat/etc/passwd
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:bin:/bin:/sbin/nologin
daemon:x:2:2:daemon:/sbin:/sbin/nologin
adm:x:3:4:adm:/var/adm:/sbin/nologin
lp:x:4:7:lp:/var/spool/lpd:/sbin/nologin
sync:x:5:0:sync:/sbin:/bin/sync
shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown
halt:x:7:0:halt:/sbin:/sbin/halt
mail:x:8:12:mail:/var/spool/mail:/sbin/nologin
operator:x:11:0:operator:/root:/sbin/nologin
games:x:12:100:games:/usr/games:/sbin/nologin
ftp:x:14:50:FTP User:/var/ftp:/sbin/nologin
nobody:x:99:99:Nobody:/:/sbin/nologin
systemd-network:x:192:192:systemd Network Management:/:/sbin/nologin
dbus:x:81:81:System message bus:/:/sbin/nologin
polkitd:x:999:998:User for polkitd:/:/sbin/nologin
sshd:x:74:74:Privilege-separated SSH:/var/empty/sshd:/sbin/nologin
postfix:x:89:89::/var/spool/postfix:/sbin/nologin
juve:x:1000:1000:juve:/home/juve:/bin/bash
toto:x:1001:1001::/home/toto:/bin/bash
```

* Creation d'un Groupe 
```bash
usermod -aG admins toto 
                 toto : toto admins
[root@localhost juve]# groups toto
```

## 2. SSH

Afin de se connecter à la machine de façon plus sécurisée, on va configurer un échange de clés SSH lorsque l'on se connecte à la machine.

Depuis la console Powershell :
```bash
 ssh-keygen -t rsa -b 4096
 ```

 A effectuer sur le serveur :
 
```bash
$ su admin_benz
$ sudo mkdir .ssh
$ sudo vim authorized_keys
$ sudo cat /home/admin_benz/.ssh/authorized_keys
ssh-rsa [...]
$ sudo chmod 600 /home/admin_benz/.ssh/authorized_keys
$ sudo chmod 700 /home/admin_benz/.ssh
```

# II. Configuration réseau

## 1. Nom de domaine
Définir un nom de domaine à la machine
```bash
$ sudo hostname juve.lab
$ hostname
juve.lab
```

## 2. Serveur DNS
Définir l'utilisation de 1.1.1.1(ou autre adresse personnalisée) comme serveur DNS de la machine.
```bash
$ sudo vim /etc/sysconfig/network-scripts/ifcfg-ens33
cat /etc/sysconfig/network-scripts/ifcfg-ens33
BOOTPROTO=static
NAME=ens33
DEVICE=ens33
IPADDR=192.168.199.10
NETMASK=255.255.255.0
DNS1=1.1.1.1
ONBOOT=yes
```

# III. Partitionnement

## 1. Préparation de la VM

Ajout de deux disques durs à la machine virtuelle, de 3Go chacun.

## 2. Partitionnement
* Ajout 2 disque de 3 Go
```bash
[root@localhost juve]# lsblk
NAME            MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda               8:0    0    8G  0 disk
├─sda1            8:1    0    1G  0 part /boot
└─sda2            8:2    0    7G  0 part
  ├─centos-root 253:0    0  6.2G  0 lvm  /
  └─centos-swap 253:1    0  820M  0 lvm  [SWAP]
sdb               8:16   0    3G  0 disk
sdc               8:32   0    3G  0 disk
sr0              11:0    1 1024M  0 rom
```
* Ajouter disque en PV:
```bash=
[root@localhost juve]# sudo pvcreate /dev/sdb
  Physical volume "/dev/sdb" successfully created.
[root@localhost juve]# sudo pvcreate /dev/sdc
  Physical volume "/dev/sdc" successfully created.
[root@localhost juve]#
```
* Agréger les deux disques en un seul volume groupe
```bash=
[root@localhost juve]#  sudo vgcreate data /dev/sdb /dev/sdc
  Volume group "data" successfully created

```
# IV. Gestion de services

* Créer un fichier qui définit une unité de service web.servicedans le répertoire /etc/systemd/system.

```bash=
vi web.service
  sudo systemctl daemon-reload
  [root@localhost system]# sudo systemctl status web
● web.service - Very simple web service
   Loaded: loaded (/etc/systemd/system/web.service; disabled; vendor preset: disabled)
   Active: inactive (dead)
[root@localhost system]# sudo systemctl start web
[root@localhost system]# sudo systemctl status web
● web.service - Very simple web service
   Loaded: loaded (/etc/systemd/system/web.service; disabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-01-06 15:42:48 CET; 13s ago
 Main PID: 3893 (python2)
   CGroup: /system.slice/web.service
           └─3893 /bin/python2 -m SimpleHTTPServer 8888
```
```bash=
[root@localhost system]# sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: dhcpv6-client ssh
  ports: 8888/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```











